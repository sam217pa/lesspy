

;;; Commentary

;; Provides functionality to change cursor color depending on context in
;; ess files.

;;; Code

;; (require 'lesspy)

(defcustom lesspy-closing-cursor-type 'hbar
  "The type of cursor when looking at a closing delimiter"
  :group 'lesspy)

(defvar lesspy--normal-cursor cursor-type
  "The normal cursor type")

(defun lesspy--set-cursor ()
  (if (lesspy--closing-p)
      (setq cursor-type lesspy-closing-cursor-type)
    (setq cursor-type lesspy--normal-cursor)))

(provide 'lesspy-cursors)

;;; end of file
