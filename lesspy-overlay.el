;;; lesspy.el --- Lispy-style bindings for ESS

;; Copyright (C) 2016 Samuel Barreto

;; Author: Samuel Barreto <samuel.barreto8@gmail.com>
;; Maintainer: Samuel Barreto <samuel.barreto8@gmail.com>
;; Version: 0.0.1
;; Package-Requires: ((ess-site "16.09") (hydra "0.13.6") (avy "0.4.0")
;; Keywords: ess

;; This file is not part of GNU Emacs.

;; This program is free software: you can redistribute it and/or
;; modify it under the terms of the GNU General Public License as
;; published by the Free Software Foundation, either version 3 of the
;; License, or (at your option) any later version.  This program is
;; distributed in the hope that it will be useful, but WITHOUT ANY
;; WARRANTY; without even the implied warranty of MERCHANTABILITY or
;; FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
;; License for more details.  You should have received a copy of the
;; GNU General Public License along with this program.  If not, see
;; <http://www.gnu.org/licenses/>.

;;; Commentary:

;; taken from the eros code, inspired by cider
;; https://github.com/xiongtx/eros/blob/master/eros.el

;;; Code:
;;;; Requires

(require 'cl-lib)

;;;; Defcustoms:

(defcustom lesspy-use-eval-overlay nil
  "Should evaluation be echoed in an overlay near the line of
code.

Defaults to nil."
  :group 'lesspy
  :type 'boolean
  :package-version '(lesspy "0.1.0"))

(defcustom lesspy-eval-result-prefix "=> "
  "The prefix displayed in the minibuffer before a result value."
  :group 'lesspy
  :type 'string
  :package-version '(lesspy "0.1.0"))

(defcustom lesspy-eval-result-duration 'command
  "Duration, in seconds, of eval-result overlays.

If nil, overlays last indefinitely.

If the symbol `command', they're erased before the next command."
  :group 'lesspy
  :type '(choice (integer :tag "Duration in seconds")
                 (const :tag "Until next command" command)
                 (const :tag "Last indefinitely" nil))
  :package-version '(lesspy "0.1.0"))

(defface lesspy-result-overlay-face
  '((((class color) (background light))
     :background "grey90" :box (:line-width -1 :color "yellow"))
    (((class color) (background dark))
     :background "grey10" :box (:line-width -1 :color "black")))
  "Face used to display evaluation results at the end of line.
This face is applied with lower priority than the syntax
highlighting."
  :group 'lesspy
  :package-version '(lesspy "0.1.0"))

(defcustom lesspy-wait-duration 50
  "Duration to wait before querying comint output, in milliseconds"
  :type 'number
  :group 'lesspy)

(defcustom lesspy-result-display 'asis
  "Type of display for results

If 'asis no overlay is displayed. If 'overlay evaluate results in
an overlay. If 'posframe evaluates results in a posframe, a child
frame of the current frame."
  :type 'symbol
  :group 'lesspy)



;;;; Functions:

(defun lesspy--await-proc ()
  "Query the current R process once every
``lesspy-wait-duration'' milliseconds until its status is not
busy."
  (let* ((proc (get-process ess-current-process-name))
         (sent (process-sentinel proc))
         (busy-p (process-get proc 'busy)))
    (if busy-p
        (progn (sleep-for 0 lesspy-wait-duration)
               (lesspy--await-proc))
      t)))

(defun lesspy--format-output (str)
  (let ((available-width (- (window-width) (current-column)))
        (str-width (string-width str))
        (clean-str (replace-regexp-in-string "\"" "" str)))
    (cond
     ((eql str-width 0) "")
     ;; [2017-11-14 13:33] FIX: do not format short string.
     ;; ((> available-width str-width)
     ;;  (setq str (replace-regexp-in-string "\n" "" str))
     ;;  (setq str (replace-regexp-in-string "^\[[0-9]+\] " "" str))
     ;;  str)
     (t clean-str))))

(defun lesspy--last-output (duration)
  "Return last evaluated function as a string"
  (lesspy--await-proc)
  (let* ((proc (get-process ess-current-process-name))
         (buf (process-buffer proc))
         (last-output
          (with-current-buffer buf
            (save-excursion
              (goto-char (max-char))
              (let ((end (point-at-bol)))
                (comint-previous-prompt 1)
                (pcase lesspy-result-display
                  ('overlay (buffer-substring-no-properties (point-at-eol) end))
                  ('posframe (buffer-substring
                              (point-at-eol) end))))))))
    (lesspy--format-output last-output)))



(defun lesspy--make-overlay (l r type &rest props)
  "Place an overlay between L and R and return it.
TYPE is a symbol put on the overlay's category property.  It is
used to easily remove all overlays from a region with:
    (remove-overlays start end 'category TYPE)
PROPS is a plist of properties and values to add to the overlay."
  (let ((o (make-overlay l (or r l) (current-buffer))))
    (overlay-put o 'category type)
    (overlay-put o 'lesspy-temporary t)
    (while props (overlay-put o (pop props) (pop props)))
    (push #'lesspy--delete-overlay
          (overlay-get o 'modification-hooks))
    o))

(defun lesspy--delete-overlay (ov &rest _)
  "Safely delete overlay OV.
Never throws errors, and can be used in an overlay's
modification-hooks."
  (ignore-errors (delete-overlay ov)))

(defun lesspy--remove-result-overlay ()
  "Remove result overlay from current buffer.
This function also removes itself from `pre-command-hook'."
  (remove-hook 'pre-command-hook #'lesspy--remove-result-overlay 'local)
  (remove-overlays nil nil 'category 'result))

(defun lesspy--eval-overlay (value point)
  "Make overlay for VALUE at POINT."
  (lesspy--make-result-overlay (format "%S" value)
    :where point
    :duration lesspy-eval-result-duration)
  value)



(cl-defun lesspy--make-result-overlay
    (value
     &rest props
     &key where duration (type 'result)
     (prepend-face 'lesspy-result-overlay-face)
     &allow-other-keys)

  (declare (indent 1))
  (while (keywordp (car props))
    (setq props (cddr props)))
  ;; If the marker points to a dead buffer, don't do anything.
  (let ((buffer (cond
                 ((markerp where) (marker-buffer where))
                 ((markerp (car-safe where)) (marker-buffer (car where)))
                 (t (current-buffer)))))
    (with-current-buffer buffer
      (save-excursion
        (when (number-or-marker-p where)
          (goto-char where))
        ;; Make sure the overlay is actually at the end of the sexp.
        (skip-chars-backward "\r\n[:blank:]")
        (let* ((beg (if (consp where)
                        (car where)
                      (save-excursion
                        (backward-sexp 1)
                        (point))))
               (end (if (consp where)
                        (cdr where)
                      (line-end-position)))
               (display-string (format "%s" value))
               (long-string-p (or (string-match "\n." display-string)
                                  (> (string-width display-string)
                                     (- (window-width) (current-column)))))
               (string-prefix (if long-string-p
                                  (concat " " lesspy-eval-result-prefix "\n")
                                (concat " " lesspy-eval-result-prefix " ")))
               (o nil))
          (remove-overlays beg end 'category type)
          (funcall #'font-lock-prepend-text-property
                   0 (length display-string)
                   'face prepend-face
                   display-string)
          (setq display-string (concat string-prefix display-string))
          ;; Put the cursor property only once we're done manipulating the
          ;; string, since we want it to be at the first char.
          (put-text-property 0 1 'cursor 0 display-string)
          ;; Create the result overlay.
          (setq o (apply #'lesspy--make-overlay
                         beg end type
                         'after-string display-string
                         props))
          (pcase duration
            ((pred numberp) (run-at-time duration nil #'lesspy--delete-overlay o))
            (`command (if this-command
                          (add-hook 'pre-command-hook
                                    #'lesspy--remove-result-overlay
                                    nil 'local)
                        (lesspy--remove-result-overlay))))
          (let ((win (get-buffer-window buffer)))
            ;; Left edge is visible.
            (when (and win
                       (<= (window-start win) (point))
                       ;; In 24.3 `<=' is still a binary predicate.
                       (<= (point) (window-end win))
                       ;; Right edge is visible. This is a little conservative
                       ;; if the overlay contains line breaks.
                       (or (< (+ (current-column) (string-width value))
                              (window-width win))
                           (not truncate-lines)))
              o)))))))

(defvar lesspy-posframe-buffer "*lesspy-eval*")

(defvar lesspy-posframe-min-width 50)

(defun lesspy--remove-result-posframe ()
  (posframe-delete lesspy-posframe-buffer))

(cl-defun lesspy--make-result-posframe
    (value &key duration &allow-other-keys)
  (pcase duration
    ((pred numberp) (run-at-time duration nil #'lesspy--delete-overlay o))
    (`command (if this-command
                  (add-hook 'pre-command-hook
                            #'lesspy--remove-result-posframe
                            nil 'local)
                (lesspy--remove-result-posframe))))
  (posframe-show
   lesspy-posframe-buffer
   :string (propertize value)
   :background-color "#073642"
   :left-fringe 5
   :right-fringe 5
   :width (window-width)))

(defun lesspy--overlay-result ()
  (lesspy--eval-overlay
   (lesspy--last-output lesspy-wait-duration)
   (point)))

(defun lesspy--eval-posframe (value point)
  "Make overlay for VALUE at POINT."
  (lesspy--make-result-posframe value
    :duration lesspy-eval-result-duration)
  value)

(defun lesspy--posframe-result ()
  (lesspy--eval-posframe
   (lesspy--last-output lesspy-wait-duration)
   (point)))

(defun lesspy--result-strategy ()
  (pcase lesspy-result-display
    ('overlay  (lesspy--overlay-result))
    ('posframe (lesspy--posframe-result))
    ('asis nil)
    (_ (error "Strategy is not one of posframe or overlay"))))


(defmacro lesspy-overlay (&rest body)
  ""
  `(progn  ,@body
           (lesspy--result-strategy)))



;;;; End of file

(provide 'lesspy-overlay)
