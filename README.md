# Lesspy

Lesspy intends to be a [`lispy`](https://github.com/abo-abo/lispy) for
R scripts.

It is a very early alpha versions but it seems to work for me.
It needs ironing and documentation too.

# License

![gpl3](https://www.gnu.org/graphics/gplv3-127x51.png)
