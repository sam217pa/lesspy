;;; lesspy.el --- Lispy-style bindings for ESS

;;; GPL 3

;; Copyright (C) 2016 Samuel Barreto

;; Author: Samuel Barreto <samuel.barreto8@gmail.com>
;; Maintainer: Samuel Barreto <samuel.barreto8@gmail.com>
;; Version: 0.0.1
;; Package-Requires: ((ess-site "16.09") (hydra "0.13.6") (avy "0.4.0") (json-navigator "0.1.0")
;; Keywords: ess

;; This file is not part of GNU Emacs.

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or (at
;; your option) any later version. This program is distributed in the
;; hope that it will be useful, but WITHOUT ANY WARRANTY; without even
;; the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
;; PURPOSE. See the GNU General Public License for more details. You
;; should have received a copy of the GNU General Public License along
;; with this program. If not, see <http://www.gnu.org/licenses/>.

;;; Commentary:

;; Lesspy provides lispy style keybindings for editing R source files
;; with Emacs Speaks Statistics.
;;
;; Lispy keybindings are context-dependant keypress that triggers
;; different commands depending on the character after or before
;; cursor.
;;
;; If `lesspy-use-keybindings' is true, then:
;;
;; - =e= after a closing delimiter evaluates the corresponding
;;   function call.
;; - =d= at a closing delimiter jumps to the corresponding
;;   opening one, and vice-versa.
;; - =a= at a delimiter jumps to another delimiter with avy.
;; - =h= at a delimiter steps up in the sexp tree at an opening
;;   delimiter, and down at a closing one. =h= at the outline regexp hides
;;   the body and shows only outline headers. If outline-minor-mode is
;;   off, does nothing.
;; - =l= at a delimiter steps down in the sexp tree at an opening
;;   delimiter, and up at a closing one. =l= at the outline regexp shows
;;   everything, does nothing if outline-minor-mode is off.
;; - =j= at a delimiter goes to next identic delimiter with same level.
;; - =k= at a delimiter goes to previous identic delimiter with same level.
;; - =p= at a closing delimiter evaluate the current block from
;;   beginning to cursor position if it is not followed by =+= (common in
;;   ggplot2) or =%>%= (common in dplyr), or evaluate the paragraph /
;;   function definition otherwise.
;; - =L= at a closing delimiter evaluates line, =C-L= at a closing
;;   delimiter evaluates line and switch to the R console.
;; - =H= at a delimiter gets help on the current function call, at an
;;   assignment operator (=<-=) gets help on the assigned object, at a
;;   pipe operator (=%>%=) gets help on the piped object.
;; - =m= at a delimiter marks sexp.
;; - =(= at a delimiter wraps the sexp in another pair of paren.
;; - =u= at a delimiter undo.
;; - =C-d= and =DEL= at a delimiter deletes sexp forward and backward respectively.
;; - =z= at a delimiter switch to inferior ess process (the R console).
;;
;; - =x= after a comma =,= or an assignement operator (=<-=) executes
;;   commonly used functions on the object before:
;;   + =xp= plots the object with /plot/.
;;   + =xq= plots the object with /qplot/.
;;   + =xh= plots the object with /hist/.
;;   + =xs= calls /str/ on the object.
;;   + =xt= calls /typeof/ on the object.
;;   + =xe= calls /head/ on the object.
;;
;; - =r= at a delimiter calls roxygen related functions:
;;   + =ru= updates the roxygen entry for the function at point.
;;   + =rr= comments out the current block with roxygen style comments.
;;   + =rh= hides all roxygen block from the buffer.
;;

;;; Code:

;;;; Requires:

(require 'hydra)
(require 'avy)
(require 'lesspy-overlay)
(require 'lesspy-tree)
(require 'lesspy-cursors)



;;;; Minor mode:

(defvar lesspy-mode-map (make-sparse-keymap))

;;;###autoload
(define-minor-mode lesspy-mode
  "Minor mode for navigating and evaluating R source code.

When `lesspy-mode' is on, some unprefixed keys conditionally call
commands instead of self-inserting. The condition is one of:

- the point is before \"(\", \"{\" or \"\\[\"
- the point is after \")\", \"}\" or \"\\]\"
  "
  :keymap lesspy-mode-map
  :group lesspy
  :lighter " lep"
  :after-hook (progn (add-hook 'post-command-hook #'lesspy--set-cursor nil t)))



;;;; Defcustoms:

(defvar ess-opening-delim "(\\|{\\|\\["
  "ess opening delimiter")

(defvar ess-closing-delim ")\\|}\\|\\]\\|[A-Za-Z\\\"]\\\""
  "ess closing delimiter")

(defcustom lesspy-use-keybindings nil
  "Use lesspy defined keybindings"
  :type 'boolean
  :group 'lesspy)

(defcustom lesspy-send-visibly nil
  "Evaluation is echoed in the r process. Defaults to nil"
  :type 'boolean
  :group 'lesspy)



;;;; Eval

(defun lesspy--clean-string (str)
  "Remove newline character inside string and strip comments"
  (let* ((str (replace-regexp-in-string "#.+$" "" str))
         ;; (str (replace-regexp-in-string "\n" "" str))
         )
    str))

(defun lesspy--get-function-or-paragraph ()
  "Returns the function or paragraph at point as a string"
  (save-excursion
    (let ((end (point)))
      (ess-goto-beginning-of-function-or-para)
      (let ((str (lesspy--clean-string
                  (buffer-substring-no-properties (point) end))))
        str))))

(defun lesspy--get-sexp ()
  "Return SEXP preceding point as a string"
  (save-excursion
    (let ((end (point)))
      (backward-list)
      (backward-sexp)
      (buffer-substring-no-properties (point) end))))

(defun lesspy--get-function-name ()
  "Return function called in SEXP at point as a string"
  (save-excursion
    (backward-list)
    (let ((end (point)))
      (backward-sexp)
      (buffer-substring (point) end))))

(defun lesspy--get-line ()
  "Return current line as string"
  (lesspy--clean-string
   (buffer-substring-no-properties
    (point-at-bol)
    (point-at-eol))))

(defun lesspy--get-outline-body ()
  "Return current outline subtree as string"
  (save-excursion
    (let ((beg (point)))
      (outline-end-of-subtree)
      (buffer-substring-no-properties beg (point)))))

(defun lesspy--send-string (str)
  "Send STR to current R process"
  (let ((proc (get-process ess-current-process-name)))
    (ess-send-string proc str lesspy-send-visibly)))

(defun lesspy--closing-p ()
  (or (looking-at " %>%")
      (and (looking-at " \\+") (looking-back ")"))
      (looking-back ess-closing-delim)))


;;;###autoload
(defun lesspy-eval-function-or-paragraph (&optional arg)
  "Eval function definition or paragraph or code when looking at
``ess-closing-delim''.

When the point is before a `%>%` character — a magrittr pipe — or
a between a closing delimiter and a `+` sign, typical in a
ggplot2 code chunk, it only eval the paragraph frome beginning to
point. It is convenient for evaluating only part of a pipeline
separately from following code."
  (interactive "p")
  (cond ((and (> arg 1)) (lesspy--closing-p)
         (lesspy-tree (lesspy--get-function-or-paragraph)))
        ((lesspy--closing-p)
         (lesspy-overlay
          (lesspy--send-string
           (lesspy--get-function-or-paragraph))))
        (t (self-insert-command arg))))

;;;###autoload
(defun lesspy-send-function-or-paragraph ()
  (interactive)
  (let ((str
         (save-excursion
           (ess-goto-end-of-function-or-para)
           (let ((end (point)))
             (ess-goto-beginning-of-function-or-para)
             (buffer-substring-no-properties (point) end)))))
    (lesspy-overlay
     (lesspy--send-string
      (lesspy--clean-string str)))))

;;;###autoload
(defun lesspy-eval-line (&optional arg)
  "Eval line of code when the point is after an
``ess-closing-delim''."
  (interactive "p")
  (cond ((and (> arg 1) (lesspy--closing-p))
         (lesspy-tree (lesspy--get-line)))
        ((lesspy--closing-p)
         (lesspy-overlay (lesspy--send-string (lesspy--get-line))))
        (t (self-insert-command arg))))

;;;###autoload
(defun lesspy-send-line ()
  "Send current line to process.
Can be called directly without context"
  (interactive)
  (lesspy-overlay
   (lesspy--send-string (lesspy--get-line))))

(defun lesspy-eval-line-and-go (arg &optional vis)
  "Eval line of code when the point is after an
``ess-closing-delim'', and switch focus to the repl."
  (interactive "p")
  (cond ((looking-back ess-closing-delim)
         (ess-eval-line-and-go vis))
        (t (self-insert-command arg))))

;;;###autoload
(defun lesspy-eval-sexp (&optional arg)
  "When point is after an ``ess-closing-delim'', eval the
function call. When point is at an outline, eval code from point
to following outline.
"
  (interactive "p")
  (cond ((looking-back ")")
         (lesspy-overlay
          (lesspy--send-string (lesspy--get-sexp))))
        ((looking-at outline-regexp)
         (lesspy--send-string (lesspy--get-outline-body)))
        (t (self-insert-command arg))))

(defun lesspy-avy-eval (&optional arg)
  (interactive "p")
  (cond ((lesspy--closing-p)
         (save-excursion
           (avy-goto-line)
           (lesspy--send-string (lesspy--get-line))))
        (t (self-insert-command arg))))

(defun lesspy-help (&optional arg)
  "Get help on function definition when point is at an
``ess-closing-delim''.

Get help on a defined object when point is before a left-arrow
operator.

Since function should be bound to `h`, it also Hide or Show
roxygen comment when point is before a roxygen comment character
`#'`.
"
  (interactive "p")
  (cond ((lesspy--closing-p)
         (ess-help (lesspy--get-function-name)))
        ((looking-back "<-")
         (save-excursion
           (backward-sexp)
           (ess-describe-object-at-point)))
        (t (self-insert-command arg))))

;;;; Move:

(defun lesspy-left (&optional arg)
  "If point is at an ``ess-closing-delim'', goes one step up in the SEXP tree.

If point is at an outline regexp, hide outline entry's body.
"
  (interactive "p")
  (cond ((looking-back ess-closing-delim)
         (backward-sexp)
         (ignore-errors (down-list)))
        ((looking-at outline-regexp)
         (cond ((bound-and-true-p outline-minor-mode)
                (outline-hide-subtree))
               (t
                (outline-minor-mode +1)
                (outline-hide-subtree))))
        (t
         (self-insert-command arg))))

(defun lesspy-right (&optional arg)
  "If point is at an ``ess-closing-delim'', goes one step down in the SEXP tree.

If point is at an outline regexp, show outline entry's body.
"
  (interactive "p")
  (cond ((region-active-p)
         (lesspy-overlay
          (ess-eval-region (region-beginning) (region-end) 'vis)))
        ((looking-back ess-closing-delim)
         (ignore-errors (up-list)))
        ((looking-at outline-regexp)
         (cond ((bound-and-true-p outline-minor-mode)
                (outline-show-subtree))
               (t
                (outline-minor-mode +1)
                (outline-show-subtree))))
        (t
         (self-insert-command arg))))

(defun lesspy-up (&optional arg)
  "If point is at outline regexp, goes to previous outline.
If point is at a roxygen comment, goes to previous roxygen
commented line. If point is at an ``ess-closing-delim'', goes to
previous leaves in the SEXP tree.
"
  (interactive "p")
  (cond ((looking-at outline-regexp)
         (outline-previous-visible-heading 1))
        ((looking-at "^#'")
         (re-search-backward "^#'" nil t))
        ((or (looking-back ess-closing-delim)
             (looking-back "%>%"))
         (ignore-errors
           (backward-list 2)
           (forward-list)))
        (t
         (self-insert-command arg))))

(defun lesspy-down (&optional arg)
  "Similar to ``lesspy-up''.

If point is at an outline regexp, goes to next outline. If point
is at a roxygen comment, goes to next roxygen-commented line. If
point is at an `ess-closing-delim', goes to next leaves in the
SEXP tree.
"
  (interactive "p")
  (cond ((looking-at outline-regexp)
         (outline-next-visible-heading 1))
        ((looking-at "^#'")
         (re-search-forward "^#'" nil t 2)
         (beginning-of-line))
        ((or (looking-back ess-closing-delim)
             (looking-back "%>%")
             (looking-at "#"))
         (ignore-errors
           (forward-list)))
        (t
         (self-insert-command arg))))

(defun lesspy-end-of-sexp ()
  "go to end of sexp"
  (interactive)
  (cond ((looking-at ess-opening-delim)
         (forward-sexp))
        (t
         (end-of-line))))

(defun lesspy-different (&optional arg)
  "If point is at an `ess-closing-delim', set point to the other side of the SEXP.
"
  (interactive "p")
  (cond ((looking-back ess-closing-delim)
         (backward-sexp))
        (t
         (self-insert-command arg))))

(defun lesspy--mark-backward-sexp ()
  (let ((end (point)))
    (backward-sexp 2)
    (set-mark (point))
    (goto-char end)))

(defun lesspy-mark (&optional arg)
  "When point is at an `ess-closing-delim', mark the SEXP."
  (interactive "p")
  (when (region-active-p) (deactivate-mark))
  (cond ((looking-back ess-closing-delim)
         (lesspy--mark-backward-sexp))
        (t
         (self-insert-command arg))))

(defun lesspy-paren (&optional arg)
  ""
  (interactive "p")
  (cond ((looking-back ess-closing-delim)
         (lesspy--mark-backward-sexp)
         (insert-parentheses))
        (t
         (self-insert-command arg))))

(defun lesspy-undo (&optional arg)
  "Undo last edit when point is at an `ess-closing-delim'."
  (interactive "p")
  (if (looking-back ess-closing-delim)
      (undo)
    (self-insert-command arg)))

(defun lesspy-kill-forward (&optional arg)
  "Kill following function call when point is at an `ess-opening-delim'.
If point is at end of line, join line with next line.
Kill region if region is active.
"
  (interactive "p")
  (cond ((region-active-p)
         (kill-region (region-beginning) (region-end)))
        ((looking-at ess-opening-delim)
         (mark-sexp)
         (kill-region (region-beginning) (region-end)))
        ;; ((looking-back ess-opening-delim)
        ;;  (set-mark (point))
        ;;  (forward-sexp 2)
        ;;  (delete-region (region-beginning) (region-end)))
        ((eq (point) (point-at-eol))
         (save-excursion
           (next-line)
           (join-line)
           (just-one-space 1)))
        (t
         (delete-char arg))))

(defun lesspy-kill-backward (&optional arg)
  "If region is active, kill region (as in default emacs).
If point is after an `ess-closing-delim', kill the SEXP.
If point is between two `\"', delete them.
If point is after a roxygen comment, delete the roxygen comment.
If point is at the end of line, join line with following line."
  (interactive "p")
  (cond ((region-active-p)
         (kill-region (region-beginning) (region-end)))
        ((looking-back ess-closing-delim)
         (lesspy--mark-backward-sexp)
         (kill-region (region-beginning) (region-end)))
        ((and (looking-at "\\\"") (looking-back "\\\""))
         (delete-char -1)
         (delete-char 1))
        ((looking-back "^## ")
         (save-excursion
           (backward-char 2)
           (delete-char -1)))
        ((looking-back "^#' \\|^#'")
         (let ((end (point)))
           (beginning-of-line)
           (delete-region (point) end)))
        (t
         (delete-backward-char 1))))

(defun lesspy-roxigen ()
  "Replace a default comment with a roxygen comment when point is
after a default code comment."
  (interactive)
  (cond ((looking-back "## ")
         (delete-backward-char 2)
         (insert "' "))
        (t
         (self-insert-command 1))))

(defun lesspy-avy-jump (&optional arg)
  "When point is after an `ess-closing-delim', jump to a leaf in
the SEXP tree within paragraph boundaries with
`avy-generic-jump'."
  (interactive "p")
  (cond ((looking-back ess-closing-delim)
         (ess-goto-beginning-of-function-or-para)
         (let ((beg (point)))
           (ess-goto-end-of-function-or-para)
           (avy--generic-jump ")\\|(" nil 'post beg (point))
           (forward-char 1)))
        (t (self-insert-command arg))))

(defun lesspy-to-shell (&optional arg)
  "Switch focus to the R REPL when point is after an
`ess-closing-delim'."
  (interactive "p")
  (cond ((looking-back ess-closing-delim)
         (ess-switch-to-inferior-or-script-buffer t))
        (t
         (self-insert-command arg))))

(defun lesspy-roxify ()
  (interactive)
  (save-excursion
    (backward-sentence)
    (set-mark (point))
    (let ((beg (point)))
      (forward-sentence)
      (ess-roxy-toggle-roxy-region beg (point)))))

(defun lesspy-execute (&optional arg)
  (interactive "p")
  (cond ((looking-back ",\\|<-")
         (hydra-lesspy-execute/body))
        ((looking-back ess-closing-delim)
         (hydra-roxify/body))
        (t
         (self-insert-command arg))))

(defun lesspy--execute (cmd)
  (interactive)
  (save-excursion
    (backward-sexp)
    (let ((sap (format "%s(%s)" cmd (ess-symbol-at-point))))
      (ess-execute sap 'buffer nil sap))))

(defhydra hydra-roxify (:exit t :hint nil :columns 3)
  "roxify"
  ("s" ess-set-working-directory "setwd")
  ("u" ess-roxy-update-entry "update")
  ("r" lesspy-roxify "roxify")
  ("h" ess-roxy-hide-all "hide")
  ("w" ess-execute-screen-options "set width")
  ("l" (lambda () (interactive) (ess-execute "list.files()" 'buffer nil "> ls")) "ls")
  ("W" (lambda () (interactive)
         (toggle-truncate-lines 1)
         (ess-execute "options(width=10000, length=99999)" 'buffer nil "full width")) "full width"))

;; TODO defun paren: quand tu tapes une parenthèse, si tu fais rien
;; après 0.2 seconde, avy te demande ou insérer la suivante. si tu
;; fais quelque chose, elle insère une paren fermante après l'ouverte
(defun lesspy-paren-wrap-next ()
  (interactive)
  (save-excursion
    (set-mark (point))
    (sp-forward-sexp)
    (insert-parentheses))
  (sp-forward-sexp)
  ;; (backward-char 1)
  )

(defhydra hydra-lesspy-execute (:exit t :hint nil :columns 4)
  "execute"
  ("p" (lambda () (interactive) (lesspy--execute "plot")) "plot")
  ("q" (lambda () (interactive) (lesspy--execute "qplot")) "qplot")
  ("h" (lambda () (interactive) (lesspy--execute "hist")) "hist")
  ("e" (lambda () (interactive) (lesspy--execute "head")) "head")
  ("s" (lambda () (interactive) (lesspy--execute "str"))  "str")
  ("t" (lambda () (interactive) (lesspy--execute "typeof")) "typeof"))

(defun lesspy-replace-regex-in-line (re1 re2)
  "replace re1 with re2 in a line"
  (beginning-of-line)
  (let ((end (line-end-position)))
    (while (re-search-forward re1 end t)
      (replace-match re2))))

(defun lesspy-cleanup-pipeline (&optional arg)
  "Clean a tidyverse / ggplot2 pipeline by adding a newline
character after every magrittr pipe and ggplot2 + in line."
  (interactive "p")
  (cond ((and (= 4 (prefix-numeric-value arg))
              (looking-back ess-closing-delim))
         (save-excursion
           (let (end (point))
             (backward-list)
             (while (re-search-forward ",\n" end t)
               (join-line)))))
        ((looking-back ess-closing-delim)
         (save-excursion
           (beginning-of-line)
           (while (re-search-forward "%>%\\|\\+\\|,\\|->\\|<-" (line-end-position) t)
             (ess-newline-and-indent))))
        (t
         (self-insert-command arg))))

(defun lesspy--comment-or-uncomment-region-or-line ()
  "Comments or uncomments the region or the current line if there's no active region."
  (let (beg end)
    (if (region-active-p)
        (setq beg (region-beginning) end (region-end))
      (setq beg (line-beginning-position) end (line-end-position)))
    (comment-or-uncomment-region beg end)))

(defun lesspy-comment (&optional arg)
  "When point is at beginning of line"
  (interactive "p")
  (cond ((eq (point) (point-at-bol))
         (cond ((or (= 4 (prefix-numeric-value arg))
                    (and (eq (point) (point-at-eol))
                         (eq (point) (point-at-bol))))
                (insert "## "))
               (t
                (save-excursion
                  (mark-paragraph)
                  (lesspy--comment-or-uncomment-region-or-line)))))
        (t
         (insert "#"))))

;;;###autoload
(defun lesspy-silence-part ()
  "Add silencing cookies `# /*` and `# */` to the beginning and
end of the current outline."
  (interactive)
  (let* ((time-string (format-time-string "%Y-%m-%d %H:%M" (current-time)))
         (opening-cookie (format "## /* %s" time-string))
         (closing-cookie (format "## %s */" time-string)))


    (cond ((region-active-p)
           ;; silence region if region is active
           (save-excursion
             (goto-char (region-beginning))
             (insert opening-cookie))
           (save-excursion
             (goto-char (region-end))
             (unless (eobp) (forward-line 1))
             (insert closing-cookie)))

          ;; silence outline subtree otherwise
          (t
           (save-excursion
             (outline-end-of-subtree)
             (insert closing-cookie))

           (save-excursion
             (outline-back-to-heading t)
             (outline-hide-subtree)
             (forward-line -1)
             (insert opening-cookie))))

    (message "%s
Added knitr spin opening and closing cookies and hidden entry subtree."
             time-string)))

(defun lesspy-outline-to-rmarkdown ()
    (interactive)
    (when (looking-at outline-regexp)
      (let* ((title (buffer-substring-no-properties
                     (point-at-bol) (point-at-eol)))
             (title-roxy (replace-regexp-in-string "##" "#'" title))
             (title-rmd  (replace-regexp-in-string "\*" "#" title-roxy)))
        (save-excursion
          (forward-line 1)
          (insert title-rmd)))))

(defun lesspy-backward-slurp ()
  "slurp sexp backward if at an opening paren"
  (interactive)
  (cond ((looking-back ess-closing-delim)
         (sp-backward-slurp-sexp))
        (t (self-insert-command arg))))

(defun lesspy-forward-slurp ()
  "slurp sexp forward if at a closing paren"
  (interactive)
  (cond ((looking-back ess-closing-delim)
         (sp-backward-down-sexp)
         (sp-slurp-hybrid-sexp)
         (sp-forward-sexp 2))
        (t (self-insert-command arg))))

(defun lesspy--mark-wip ()
  (let ((beg (re-search-backward "^## \/\\*"))
        (end (re-search-forward "^## .+\\*\/")))
    (set-mark beg)
    (goto-char end)))

(defun lesspy-kill-wip ()
  (interactive)
  (lesspy--mark-wip)
  (kill-region (region-beginning)
               (region-end)))

(defun lesspy-eval-wip ()
  (interactive)
  (lesspy--mark-wip)
  (let* ((beg (region-beginning))
         (end (region-end))
         (substr (buffer-substring-no-properties beg end))
         (cleaned (lesspy--clean-string substr)))
    (lesspy-overlay
     (lesspy--send-string cleaned))))

;;; Keybindings

(when lesspy-use-keybindings
  (let ((map lesspy-mode-map))
    (define-key map (kbd "e") 'lesspy-eval-sexp)
    (define-key map (kbd "d") 'lesspy-different)
    (define-key map (kbd "a") 'lesspy-avy-jump)
    (define-key map (kbd "h") 'lesspy-left)
    (define-key map (kbd "l") 'lesspy-right)
    (define-key map (kbd "j") 'lesspy-down)
    (define-key map (kbd "k") 'lesspy-up)
    (define-key map (kbd "p") 'lesspy-eval-function-or-paragraph)
    (define-key map (kbd "L") 'lesspy-eval-line)
    (define-key map (kbd "C-L") 'lesspy-eval-line-and-go)
    (define-key map (kbd "H") 'lesspy-help)
    (define-key map (kbd "m") 'lesspy-mark)
    (define-key map (kbd "u") 'lesspy-undo)
    (define-key map (kbd "(") 'lesspy-paren)
    (define-key map (kbd "C-d") 'lesspy-delete-forward)
    (define-key map (kbd "DEL") 'lesspy-delete-backward)
    (define-key map (kbd "z") 'lesspy-to-shell)
    (define-key map (kbd "x") 'lesspy-execute)))

;;; End of File:

(provide 'lesspy)
