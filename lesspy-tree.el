;;; lesspy-tree.el - explore data structures with a tree

;;; GPL 3

;; Copyright (C) 2017 Samuel Barreto <samuel.barreto8@gmail.com>
;;
;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation; either version 3 of the License, or
;; (at your option) any later version.
;;
;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License
;; along with this program; if not, write to the Free Software
;; Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
;;

;;; Commentary:

;; Provides functionality to explore list data structures inside emacs
;; with the ``hierarchy.el'' package.

;;; Code:

;;;; Requires:

(require 'json-navigator)
(require 'lesspy-overlay)

;;;; Defcustoms & Defvars

(defcustom lesspy-tree-buffer "*lesspy-tree*"
  "Name of the buffer in which the tree is displayed."
  :type 'string
  :group 'lesspy)

;;;; Defuns:

(defun lesspy--navigator-display-tree (json)
  "Display hierarchy of JSON in a tree widget."
  (switch-to-buffer
   (hierarchy-tree-display
    (json-navigator-create-hierarchy json)
    (lambda (item _) (json-navigator--insert (json-navigator--unwrap item)))
    (get-buffer-create lesspy-tree-buffer))))

(defun lesspy--navigator-navigate-region (&optional start end)
  "Navigate JSON inside region between START and END.
If START (respectively END) is nil, use `point-min' (respectively
`point-max') instead."
  (interactive "r")
  (let ((start (or start (point-min)))
        (end (or end (point-max))))
    (lesspy--navigator-display-tree
     (json-navigator--read-region start end))))

;;;###autoload
(defun lesspy-tree (str)
  (interactive)
  (let* ((proc (get-process ess-current-process-name))
         (buf (process-buffer proc))
         (str (format "jsonlite::toJSON(as.list(%s), pretty = TRUE)" str)))
    (ess-send-string proc str)
    (lesspy--await-proc)
    (with-current-buffer buf
      (save-excursion
        (goto-char (max-char))
        (let ((end (point-at-bol)))
          (comint-previous-prompt 1)
          (lesspy--navigator-navigate-region
           (point-at-eol) end))))))

(provide 'lesspy-tree)
;;; end of file
